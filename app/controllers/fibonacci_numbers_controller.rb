class FibonacciNumbersController < ApplicationController
  def index
    begin
      result = JSON.parse(REDIS.get('fib_answer'))
    rescue JSON::ParserError, TypeError
      @last_fib_answer = 'not found'
      return
    end

    @last_fib_answer = "#{result['input']} => #{result['output']}"
  end

  def create
    FibonacciJob.perform_later params[:fib]
    redirect_to fibonacci_numbers_path
  end
end
