class FibonacciJob < ApplicationJob
  queue_as :sample_app_default

  def perform(n)
    REDIS.set('fib_answer', { input: n, output: slow_fib(n.to_i) }.to_json)
  end

  private

  def slow_fib(n)
    return n if n <= 1
    return slow_fib(n-1) + slow_fib(n-2)
  end
end
