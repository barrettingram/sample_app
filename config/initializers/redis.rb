case Rails.env
when 'production'
  REDIS = Redis.new(url: ENV['REDIS_URL'])
else # default to localhost in development
  REDIS = Redis.new
end
